//request type: POST (remove this comment before use in ESB)
var requestObj = mc.getPayloadJSON();
var request = " <util:SendEmailReqMsg xmlns:util=\"http://www.huawei.com/bss/soaif/interface/UtilityService/\" xmlns:com=\"http://www.huawei.com/bss/soaif/interface/common/\"> " +
    "<com:ReqHeader>";
if (typeof (requestObj.ReqHeader.Version) != "undefined") {
    request = request + "<com:Version>" + requestObj.ReqHeader.Version + "</com:Version> ";
}

if (typeof (requestObj.ReqHeader.BusinessCode) != "undefined") {
    request = request + "<com:BusinessCode>" + requestObj.ReqHeader.BusinessCode + "</com:BusinessCode> ";
}

request = request + "<com:TransactionId>" + requestObj.ReqHeader.TransactionId + "</com:TransactionId> ";

if (typeof (requestObj.ReqHeader.Channel) != "undefined") {
    request = request + "<com:Channel>" + requestObj.ReqHeader.Channel + "</com:Channel> ";
}

if (typeof (requestObj.ReqHeader.PartnerId) != "undefined") {
    request = request + "<com:PartnerId>" + requestObj.ReqHeader.PartnerId + "</com:PartnerId> ";
}

if (typeof (requestObj.ReqHeader.BrandId) != "undefined") {
    request = request + "<com:BrandId>" + requestObj.ReqHeader.BrandId + "</com:BrandId> ";
}

request = request + "<com:ReqTime>" + requestObj.ReqHeader.ReqTime + "</com:ReqTime> ";

if (typeof (requestObj.ReqHeader.TimeFormat) != "undefined") {
    request = request + "<com:TimeFormat>";
    request = request + "<com:TimeType>" + requestObj.ReqHeader.TimeFormat.TimeType + "</com:TimeType> ";
    if (typeof (requestObj.ReqHeader.TimeFormat.TimeZoneID) != "undefined") {
        request = request + "<com:TimeZoneID>" + requestObj.ReqHeader.TimeFormat.TimeZoneID + "</com:TimeZoneID> ";
    }
    request = request + "</com:TimeFormat>";
}


request = request + "<com:AccessUser>" + requestObj.ReqHeader.AccessUser + "</com:AccessUser> ";

request = request + "<com:AccessPassword>" + requestObj.ReqHeader.AccessPassword + "</com:AccessPassword> ";

if (typeof (requestObj.ReqHeader.OperatorId) != "undefined") {
    request = request + "<com:OperatorId>" + requestObj.ReqHeader.OperatorId + "</com:OperatorId> ";
}

if (typeof(requestObj.ReqHeader.AdditionalProperty) != "undefined") {
    for (i = 0; i < requestObj.ReqHeader.AdditionalProperty.length; i++) {
        request = request + "<com:AdditionalProperty> " +
            "<com:Code>" + requestObj.ReqHeader.AdditionalProperty[i].Code + "</com:Code>" +
            "<com:Value>" + requestObj.ReqHeader.AdditionalProperty[i].Value + "</com:Value>" +
            "</com:AdditionalProperty>";
    }
}
request = request + "</com:ReqHeader>";

if (typeof (requestObj.EmailInfo) != "undefined") {
    for (var i = 0; i < requestObj.EmailInfo.length; i++) {
        request = request + "<util:EmailInfo>";
        if (typeof (requestObj.EmailInfo[i].BatchSeqId) != "undefined") {
            request = request + "<util:BatchSeqId>" + requestObj.EmailInfo[i].BatchSeqId + "</util:BatchSeqId>";
        }
        if (typeof (requestObj.EmailInfo[i].Subject) != "undefined") {
            request = request + "<util:Subject>" + requestObj.EmailInfo[i].Subject + "</util:Subject>";
        }
        if (typeof (requestObj.EmailInfo[i].Content) != "undefined") {
            request = request + "<util:Content>" + requestObj.EmailInfo[i].Content + "</util:Content>";
        }
        for (var j = 0; j < requestObj.EmailInfo[i].Recipient.length; j++) {
            request = request + "<util:Recipient>" + requestObj.EmailInfo[i].Recipient[j] + "</util:Recipient>";
        }
        if (typeof (requestObj.EmailInfo[i].Sender) != "undefined") {
            request = request + "<util:Sender>" + requestObj.EmailInfo[i].Sender + "</util:Sender>";
        }
        if (typeof (requestObj.EmailInfo[i].AttachmentPath) != "undefined") {
            request = request + "<util:AttachmentPath>" + requestObj.EmailInfo[i].AttachmentPath + "</util:AttachmentPath>";
        }

        request = request + "</util:EmailInfo>";
    }
}

request = request + "</util:SendEmailReqMsg>";

mc.setPayloadXML(new XML(request));
