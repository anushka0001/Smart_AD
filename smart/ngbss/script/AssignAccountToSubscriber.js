// request type: POST (remove this comment before use in ESB)

var requestObj = mc.getPayloadJSON();
var request = " <sub:AssignAcctToSubReqMsg xmlns:sub=\"http://www.huawei.com/bss/soaif/interface/SubscriberService/\" xmlns:com=\"http://www.huawei.com/bss/soaif/interface/common/\"> " +
    "<com:ReqHeader> ";
if (typeof (requestObj.ReqHeader.Version) != "undefined") {
    request = request + "<com:Version>" + requestObj.ReqHeader.Version + "</com:Version> ";
}

if (typeof (requestObj.ReqHeader.BusinessCode) != "undefined") {
    request = request + "<com:BusinessCode>" + requestObj.ReqHeader.BusinessCode + "</com:BusinessCode> ";
}

request = request + "<com:TransactionId>" + requestObj.ReqHeader.TransactionId + "</com:TransactionId> ";

if (typeof (requestObj.ReqHeader.Channel) != "undefined") {
    request = request + "<com:Channel>" + requestObj.ReqHeader.Channel + "</com:Channel> ";
}

if (typeof (requestObj.ReqHeader.PartnerId) != "undefined") {
    request = request + "<com:PartnerId>" + requestObj.ReqHeader.PartnerId + "</com:PartnerId> ";
}

if (typeof (requestObj.ReqHeader.BrandId) != "undefined") {
    request = request + "<com:BrandId>" + requestObj.ReqHeader.BrandId + "</com:BrandId> ";
}

request = request + "<com:ReqTime>" + requestObj.ReqHeader.ReqTime + "</com:ReqTime> ";

if (typeof (requestObj.ReqHeader.TimeFormat) != "undefined") {
    request = request + "<com:TimeFormat>";
    request = request + "<com:TimeType>" + requestObj.ReqHeader.TimeFormat.TimeType + "</com:TimeType> ";
    if (typeof (requestObj.ReqHeader.TimeFormat.TimeZoneID) != "undefined") {
        request = request + "<com:TimeZoneID>" + requestObj.ReqHeader.TimeFormat.TimeZoneID + "</com:TimeZoneID> ";
    }
    request = request + "</com:TimeFormat>";
}


request = request + "<com:AccessUser>" + requestObj.ReqHeader.AccessUser + "</com:AccessUser> ";

request = request + "<com:AccessPassword>" + requestObj.ReqHeader.AccessPassword + "</com:AccessPassword> ";

if (typeof (requestObj.ReqHeader.OperatorId) != "undefined") {
    request = request + "<com:OperatorId>" + requestObj.ReqHeader.OperatorId + "</com:OperatorId> ";
}

if (typeof(requestObj.ReqHeader.AdditionalProperty) != "undefined") {
    for (i = 0; i < requestObj.ReqHeader.AdditionalProperty.length; i++) {
        request = request + "<com:AdditionalProperty> " +
            "<com:Code>" + requestObj.ReqHeader.AdditionalProperty[i].Code + "</com:Code>" +
            "<com:Value>" + requestObj.ReqHeader.AdditionalProperty[i].Value + "</com:Value>" +
            "</com:AdditionalProperty>";
    }
}
request = request + "</com:ReqHeader>";

if (typeof (requestObj.AccessInfo) != "undefined") {
    request = request + "<sub:AccessInfo>";
    if (typeof (requestObj.AccessInfo.ObjectIdType) != "undefined") {
        request = request + "<com:ObjectIdType>" + requestObj.AccessInfo.ObjectIdType + "</com:ObjectIdType>";
    }
    if (typeof (requestObj.AccessInfo.ObjectId) != "undefined") {
        request = request + "<com:ObjectId>" + requestObj.AccessInfo.ObjectId + "</com:ObjectId>";
    }
    request = request + "</sub:AccessInfo>";
}

if (typeof (requestObj.AcctInfo) != "undefined") {
    request = request + "<sub:AcctInfo>";
    if (typeof (requestObj.AcctInfo.ExistingAcctId) != "undefined") {
        request = request + "<sub:ExistingAcctId>" + requestObj.AcctInfo.ExistingAcctId + "</sub:ExistingAcctId>";
    }
    if (typeof (requestObj.AcctInfo.Account) != "undefined") {
        request = request + "<sub:Account>";
        if (typeof (requestObj.AcctInfo.Account.CustId) != "undefined") {
            request = request + "<sub:CustId>" + requestObj.AcctInfo.Account.CustId + "</sub:CustId>";
        }
        if (typeof (requestObj.AcctInfo.Account.AcctName) != "undefined") {
            request = request + "<sub:AcctName>" + requestObj.AcctInfo.Account.AcctName + "</sub:AcctName>";
        }
        if (typeof (requestObj.AcctInfo.Account.PaymentType) != "undefined") {
            request = request + "<sub:PaymentType>" + requestObj.AcctInfo.Account.PaymentType + "</sub:PaymentType>";
        }
        if (typeof (requestObj.AcctInfo.Account.Title) != "undefined") {
            request = request + "<sub:Title>" + requestObj.AcctInfo.Account.Title + "</sub:Title>";
        }
        if (typeof (requestObj.AcctInfo.Account.Name) != "undefined") {
            request = request + "<sub:Name>";
            if (typeof (requestObj.AcctInfo.Account.Name.FirstName) != "undefined") {
                request = request + "<com:FirstName>" + requestObj.AcctInfo.Account.Name.FirstName + "</com:FirstName>";
            }
            if (typeof (requestObj.AcctInfo.Account.Name.MiddleName) != "undefined") {
                request = request + "<com:MiddleName>" + requestObj.AcctInfo.Account.Name.MiddleName + "</com:MiddleName>";
            }
            if (typeof (requestObj.AcctInfo.Account.Name.LastName) != "undefined") {
                request = request + "<com:LastName>" + requestObj.AcctInfo.Account.Name.LastName + "</com:LastName>";
            }

            request = request + "</sub:Name>";
        }

        if (typeof (requestObj.AcctInfo.Account.BillCycleType) != "undefined") {
            request = request + "<sub:BillCycleType>" + requestObj.AcctInfo.Account.BillCycleType + "</sub:BillCycleType>";
        }
        if (typeof (requestObj.AcctInfo.Account.BillLanguage) != "undefined") {
            request = request + "<sub:BillLanguage>" + requestObj.AcctInfo.Account.BillLanguage + "</sub:BillLanguage>";
        }
        if (typeof (requestObj.AcctInfo.Account.Contact) != "undefined") {
            request = request + "<sub:Contact>";
            if (typeof (requestObj.AcctInfo.Account.Contact.ContactId) != "undefined") {
                request = request + "<com:ContactId>" + requestObj.AcctInfo.Account.Contact.ContactId + "</com:ContactId>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.Priority) != "undefined") {
                request = request + "<com:Priority>" + requestObj.AcctInfo.Account.Contact.Priority + "</com:Priority>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.ContactType) != "undefined") {
                request = request + "<com:ContactType>" + requestObj.AcctInfo.Account.Contact.ContactType + "</com:ContactType>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.Title) != "undefined") {
                request = request + "<com:Title>" + requestObj.AcctInfo.Account.Contact.Title + "</com:Title>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.Name) != "undefined") {
                request = request + "<sub:Name>";
                if (typeof (requestObj.AcctInfo.Account.Contact.Name.FirstName) != "undefined") {
                    request = request + "<com:FirstName>" + requestObj.AcctInfo.Account.Contact.Name.FirstName + "</com:FirstName>";
                }
                if (typeof (requestObj.AcctInfo.Account.Contact.Name.MiddleName) != "undefined") {
                    request = request + "<com:MiddleName>" + requestObj.AcctInfo.Account.Contact.Name.MiddleName + "</com:MiddleName>";
                }
                if (typeof (requestObj.AcctInfo.Account.Contact.Name.LastName) != "undefined") {
                    request = request + "<com:LastName>" + requestObj.AcctInfo.Account.Contact.Name.LastName + "</com:LastName>";
                }

                request = request + "</sub:Name>";
            }

            if (typeof (requestObj.AcctInfo.Account.Contact.OfficePhone) != "undefined") {
                request = request + "<com:OfficePhone>" + requestObj.AcctInfo.Account.Contact.OfficePhone + "</com:OfficePhone>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.HomePhone) != "undefined") {
                request = request + "<com:HomePhone>" + requestObj.AcctInfo.Account.Contact.HomePhone + "</com:HomePhone>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.MobilePhone) != "undefined") {
                request = request + "<com:MobilePhone>" + requestObj.AcctInfo.Account.Contact.MobilePhone + "</com:MobilePhone>";
            }
            if (typeof (requestObj.AcctInfo.Account.Contact.Email) != "undefined") {
                request = request + "<com:Email>" + requestObj.AcctInfo.Account.Contact.Email + "</com:Email>";
            }

            request = request + "</sub:Contact>";
        }

        if (typeof (requestObj.AcctInfo.Account.Address) != "undefined") {
            for (var i = 0; i < requestObj.AcctInfo.Account.Address.length; i++) {
                request = request + "<sub:Address>";
                if (typeof (requestObj.AcctInfo.Account.Address[i].AddressId) != "undefined") {
                    request = request + "<com:AddressId>" + requestObj.AcctInfo.Account.Address[i].AddressId + "</com:AddressId>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].AddressType) != "undefined") {
                    request = request + "<com:AddressType>" + requestObj.AcctInfo.Account.Address[i].AddressType + "</com:AddressType>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address1) != "undefined") {
                    request = request + "<com:Address1>" + requestObj.AcctInfo.Account.Address[i].Address1 + "</com:Address1>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address2) != "undefined") {
                    request = request + "<com:Address2>" + requestObj.AcctInfo.Account.Address[i].Address2 + "</com:Address2>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address3) != "undefined") {
                    request = request + "<com:Address3>" + requestObj.AcctInfo.Account.Address[i].Address3 + "</com:Address3>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address4) != "undefined") {
                    request = request + "<com:Address4>" + requestObj.AcctInfo.Account.Address[i].Address4 + "</com:Address4>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address5) != "undefined") {
                    request = request + "<com:Address5>" + requestObj.AcctInfo.Account.Address[i].Address5 + "</com:Address5>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address6) != "undefined") {
                    request = request + "<com:Address6>" + requestObj.AcctInfo.Account.Address[i].Address6 + "</com:Address6>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address7) != "undefined") {
                    request = request + "<com:Address7>" + requestObj.AcctInfo.Account.Address[i].Address7 + "</com:Address7>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address8) != "undefined") {
                    request = request + "<com:Address8>" + requestObj.AcctInfo.Account.Address[i].Address8 + "</com:Address8>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address9) != "undefined") {
                    request = request + "<com:Address9>" + requestObj.AcctInfo.Account.Address[i].Address9 + "</com:Address9>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address10) != "undefined") {
                    request = request + "<com:Address10>" + requestObj.AcctInfo.Account.Address[i].Address10 + "</com:Address10>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address11) != "undefined") {
                    request = request + "<com:Address11>" + requestObj.AcctInfo.Account.Address[i].Address11 + "</com:Address11>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].Address12) != "undefined") {
                    request = request + "<com:Address12>" + requestObj.AcctInfo.Account.Address[i].Address12 + "</com:Address12>";
                }
                if (typeof (requestObj.AcctInfo.Account.Address[i].PostCode) != "undefined") {
                    request = request + "<com:PostCode>" + requestObj.AcctInfo.Account.Address[i].PostCode + "</com:PostCode>";
                }


                request = request + "</sub:Address>";
            }
        }

        if (typeof (requestObj.AcctInfo.Account.BillMedium) != "undefined") {
            for (var i = 0; i < requestObj.AcctInfo.Account.BillMedium.length; i++) {
                request = request + "<sub:BillMedium>";
                if (typeof (requestObj.AcctInfo.Account.BillMedium[i].BillMediumId) != "undefined") {
                    request = request + "<com:BillMediumId>" + requestObj.AcctInfo.Account.BillMedium[i].BillMediumId + "</com:BillMediumId>";
                }
                if (typeof (requestObj.AcctInfo.Account.BillMedium[i].BillMediumCode) != "undefined") {
                    request = request + "<com:BillMediumCode>" + requestObj.AcctInfo.Account.BillMedium[i].BillMediumCode + "</com:BillMediumCode>";
                }
                if (typeof (requestObj.AcctInfo.Account.BillMedium[i].BillContentType) != "undefined") {
                    request = request + "<com:BillContentType>" + requestObj.AcctInfo.Account.BillMedium[i].BillContentType + "</com:BillContentType>";
                }
                if (typeof (requestObj.AcctInfo.Account.BillMedium[i].BillMediumInfo) != "undefined") {
                    request = request + "<com:BillMediumInfo>" + requestObj.AcctInfo.Account.BillMedium[i].BillMediumInfo + "</com:BillMediumInfo>";
                }
                request = request + "</sub:BillMedium>";
            }
        }

        if (typeof (requestObj.AcctInfo.Account.Currency) != "undefined") {
            request = request + " <sub:Currency>" + requestObj.AcctInfo.Account.Currency + "</sub:Currency>";
        }
        if (typeof (requestObj.AcctInfo.Account.InitialBalance) != "undefined") {
            request = request + " <sub:InitialBalance>" + requestObj.AcctInfo.Account.InitialBalance + "</sub:InitialBalance>";
        }
        if (typeof (requestObj.AcctInfo.Account.InitialBalance) != "undefined") {
            request = request + " <sub:InitialBalance>" + requestObj.AcctInfo.Account.InitialBalance + "</sub:InitialBalance>";
        }

        if (typeof (requestObj.AcctInfo.Account.CreditLimit) != "undefined") {
            for (var i = 0; i < requestObj.AcctInfo.Account.CreditLimit.length; i++) {
                request = request + "<sub:CreditLimit>";
                if (typeof (requestObj.AcctInfo.Account.CreditLimit[i].LimitType) != "undefined") {
                    request = request + "<com:LimitType>" + requestObj.AcctInfo.Account.CreditLimit[i].LimitType + "</com:LimitType>";
                }
                if (typeof (requestObj.AcctInfo.Account.CreditLimit[i].LimitValue) != "undefined") {
                    request = request + "<com:LimitValue>" + requestObj.AcctInfo.Account.CreditLimit[i].LimitValue + "</com:LimitValue>";
                }
                request = request + "</sub:CreditLimit>";
            }
        }

        if (typeof (requestObj.AcctInfo.Account.AcctPayMethod) != "undefined") {
            request = request + " <sub:AcctPayMethod>" + requestObj.AcctInfo.Account.AcctPayMethod + "</sub:AcctPayMethod>";
        }

        if (typeof (requestObj.AcctInfo.Account.PaymentChannel) != "undefined") {
            for (var i = 0; i < requestObj.AcctInfo.Account.PaymentChannel.length; i++) {
                request = request + "<sub:PaymentChannel>";
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].PaymentId) != "undefined") {
                    request = request + "<com:PaymentId>" + requestObj.AcctInfo.Account.PaymentChannel[i].PaymentId + "</com:PaymentId>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].AcctType) != "undefined") {
                    request = request + "<com:AcctType>" + requestObj.AcctInfo.Account.PaymentChannel[i].AcctType + "</com:AcctType>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].AcctNum) != "undefined") {
                    request = request + "<com:AcctNum>" + requestObj.AcctInfo.Account.PaymentChannel[i].AcctNum + "</com:AcctNum>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].AcctName) != "undefined") {
                    request = request + "<com:AcctName>" + requestObj.AcctInfo.Account.PaymentChannel[i].AcctName + "</com:AcctName>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].BankCode) != "undefined") {
                    request = request + "<com:BankCode>" + requestObj.AcctInfo.Account.PaymentChannel[i].BankCode + "</com:BankCode>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].BankBranchCode) != "undefined") {
                    request = request + "<com:BankBranchCode>" + requestObj.AcctInfo.Account.PaymentChannel[i].BankBranchCode + "</com:BankBranchCode>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].CreditCardType) != "undefined") {
                    request = request + "<com:CreditCardType>" + requestObj.AcctInfo.Account.PaymentChannel[i].CreditCardType + "</com:CreditCardType>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].CVVNum) != "undefined") {
                    request = request + "<com:CVVNum>" + requestObj.AcctInfo.Account.PaymentChannel[i].CVVNum + "</com:CVVNum>";
                }
                if (typeof (requestObj.AcctInfo.Account.PaymentChannel[i].ExpireDate) != "undefined") {
                    request = request + "<com:ExpireDate>" + requestObj.AcctInfo.Account.PaymentChannel[i].ExpireDate + "</com:ExpireDate>";
                }

                request = request + "</sub:PaymentChannel>";
            }
        }


        if (typeof (requestObj.AcctInfo.Account.AdditionalProperty) != "undefined") {
            for (var i = 0; i < requestObj.AcctInfo.Account.AdditionalProperty.length; i++) {
                request = request + "<sub:AdditionalProperty>";
                if (typeof (requestObj.AcctInfo.Account.AdditionalProperty[i].Code) != "undefined") {
                    request = request + "<com:Code>" + requestObj.AcctInfo.Account.AdditionalProperty[i].Code + "</com:Code>";
                }
                if (typeof (requestObj.AcctInfo.Account.AdditionalProperty[i].Value) != "undefined") {
                    request = request + "<com:Value>" + requestObj.AcctInfo.Account.AdditionalProperty[i].Value + "</com:Value>";
                }
                request = request + "</sub:AdditionalProperty>";
            }
        }

        request = request + "</sub:Account>";
    }

    request = request + "</sub:AcctInfo>";
}

request = request + "</sub:AssignAcctToSubReqMsg>";

mc.setPayloadXML(new XML(request));