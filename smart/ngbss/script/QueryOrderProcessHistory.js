//request type: GET (remove this comment before use in ESB)

var version = mc.getProperty('query.param.version');
var businessCode = mc.getProperty('query.param.businessCode');
var transactionId = mc.getProperty('query.param.transactionId');
var channel = mc.getProperty('query.param.channel');
var partnerId = mc.getProperty('query.param.partnerId');
var brandId = mc.getProperty('query.param.brandId');
var reqTime = mc.getProperty('query.param.reqTime');
var timeType = mc.getProperty('query.param.timeType');
var timeZoneID = mc.getProperty('query.param.timeZoneID');
var accessUser = mc.getProperty('query.param.accessUser');
var accessPassword = mc.getProperty('query.param.accessPassword');
var operatorId = mc.getProperty('query.param.operatorId');
var code = mc.getProperty('query.param.code');
var value = mc.getProperty('query.param.value');
var orderId = mc.getProperty('query.param.orderId');


var valueSet = new Array();
var codeSet = new Array();


if (!!code) {
    codeSet = code.split(",");
}
if (!!value) {
    valueSet = value.split(",");
}


var request = "<ord:QueryOrderReqMsg xmlns:ord=\"http://www.huawei.com/bss/soaif/interface/OrderService/\" xmlns:com=\"http://www.huawei.com/bss/soaif/interface/common/\">" +
    "<com:ReqHeader>";

if ((!!version)) {
    request = request + " <com:Version>" + version + "</com:Version>";
}

if ((!!businessCode)) {
    request = request + "<com:BusinessCode>" + businessCode + "</com:BusinessCode>";
}

if (!!transactionId) {
    request = request + "<com:TransactionId>" + transactionId + "</com:TransactionId>";
}

if ((!!channel)) {
    request = request + " <com:Channel>" + channel + "</com:Channel>";
}

if ((!!partnerId)) {
    request = request + "<com:PartnerId>" + partnerId + "</com:PartnerId>";
}

if ((!!brandId)) {
    request = request + " <com:BrandId>" + brandId + "</com:BrandId>";
}

if (!!reqTime) {
    request = request + "<com:ReqTime>" + reqTime + "</com:ReqTime>";
}

if ((!!timeType)) {
    request = request + "<com:TimeFormat>" +
        "<com:TimeType>timeType</com:TimeType>";

    if ((!!timeZoneID)) {
        request = request + "<com:TimeZoneID>timeZoneID</com:TimeZoneID>";
    }
    request = request + "</com:TimeFormat>";
}

request = request + "<com:AccessUser>" + accessUser + "</com:AccessUser>";
request = request + "<com:AccessPassword>" + accessPassword + "</com:AccessPassword>";

if ((!!operatorId)) {
    request = request + "<com:OperatorId>" + operatorId + "</com:OperatorId>";
}

if ((codeSet.length) == (valueSet.length)) {
    for (i = 0; i < codeSet.length; i++) {
        request = request + "<com:AdditionalProperty> ";
        request = request + "<com:Code>" + codeSet[i] + "</com:Code>";
        request = request + "<com:Value>" + valueSet[i] + "</com:Value>";
        request = request + "</com:AdditionalProperty> ";
    }
}

request = request + "  </com:ReqHeader>";

if (!!orderId) {
    request = request + "<ord:OrderId>" + orderId + "</ord:OrderId>";
}
request = request + "</ord:QueryOrderProcessHistoryReqMsg>";

mc.setPayloadXML(new XML(request));