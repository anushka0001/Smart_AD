//request type: PUT (remove this comment before use in ESB)

var requestObj = mc.getPayloadJSON();
var request = " <sub:ChangeSubStatusReqMsg xmlns:sub=\"http://www.huawei.com/bss/soaif/interface/SubscriberService/\" xmlns:com=\"http://www.huawei.com/bss/soaif/interface/common/\"> " +
    "<com:ReqHeader> ";
if (typeof (requestObj.ReqHeader.Version) != "undefined") {
    request = request + "<com:Version>" + requestObj.ReqHeader.Version + "</com:Version> ";
}

if (typeof (requestObj.ReqHeader.BusinessCode) != "undefined") {
    request = request + "<com:BusinessCode>" + requestObj.ReqHeader.BusinessCode + "</com:BusinessCode> ";
}

request = request + "<com:TransactionId>" + requestObj.ReqHeader.TransactionId + "</com:TransactionId> ";

if (typeof (requestObj.ReqHeader.Channel) != "undefined") {
    request = request + "<com:Channel>" + requestObj.ReqHeader.Channel + "</com:Channel> ";
}

if (typeof (requestObj.ReqHeader.PartnerId) != "undefined") {
    request = request + "<com:PartnerId>" + requestObj.ReqHeader.PartnerId + "</com:PartnerId> ";
}

if (typeof (requestObj.ReqHeader.BrandId) != "undefined") {
    request = request + "<com:BrandId>" + requestObj.ReqHeader.BrandId + "</com:BrandId> ";
}

request = request + "<com:ReqTime>" + requestObj.ReqHeader.ReqTime + "</com:ReqTime> ";

if (typeof (requestObj.ReqHeader.TimeFormat) != "undefined") {
    request = request + "<com:TimeFormat>";
    request = request + "<com:TimeType>" + requestObj.ReqHeader.TimeFormat.TimeType + "</com:TimeType> ";
    if (typeof (requestObj.ReqHeader.TimeFormat.TimeZoneID) != "undefined") {
        request = request + "<com:TimeZoneID>" + requestObj.ReqHeader.TimeFormat.TimeZoneID + "</com:TimeZoneID> ";
    }
    request = request + "</com:TimeFormat>";
}


request = request + "<com:AccessUser>" + requestObj.ReqHeader.AccessUser + "</com:AccessUser> ";

request = request + "<com:AccessPassword>" + requestObj.ReqHeader.AccessPassword + "</com:AccessPassword> ";

if (typeof (requestObj.ReqHeader.OperatorId) != "undefined") {
    request = request + "<com:OperatorId>" + requestObj.ReqHeader.OperatorId + "</com:OperatorId> ";
}

if (typeof(requestObj.ReqHeader.AdditionalProperty) != "undefined") {
    for (i = 0; i < requestObj.ReqHeader.AdditionalProperty.length; i++) {
        request = request + "<com:AdditionalProperty> " +
            "<com:Code>" + requestObj.ReqHeader.AdditionalProperty[i].Code + "</com:Code>" +
            "<com:Value>" + requestObj.ReqHeader.AdditionalProperty[i].Value + "</com:Value>" +
            "</com:AdditionalProperty>";
    }
}
request = request + "</com:ReqHeader>";

if (typeof (requestObj.AccessInfo) != "undefined") {
    request = request + "<sub:AccessInfo>";
    if (typeof (requestObj.AccessInfo.ObjectIdType) != "undefined") {
        request = request + "<com:ObjectIdType>" + requestObj.AccessInfo.ObjectIdType + "</com:ObjectIdType>";
    }
    if (typeof (requestObj.AccessInfo.ObjectId) != "undefined") {
        request = request + "<com:ObjectId>" + requestObj.AccessInfo.ObjectId + "</com:ObjectId>";
    }
    request = request + "</sub:AccessInfo>";
}

if (typeof (requestObj.OperateType) != "undefined") {
    request = request + "<sub:OperateType>" + requestObj.OperateType + "</sub:OperateType>";
}

if (typeof (requestObj.ReasonCode) != "undefined") {
    request = request + "<sub:ReasonCode>" + requestObj.ReasonCode + "</sub:ReasonCode>";
}

request = request + "</sub:ChangeSubStatusReqMsg>";

mc.setPayloadXML(new XML(request));