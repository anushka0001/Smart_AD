//Request type: GET (remove this comment before use in ESB)

var version = mc.getProperty('query.param.version');
var businessCode = mc.getProperty('query.param.businessCode');
var transactionId = mc.getProperty('query.param.transactionId');
var channel = mc.getProperty('query.param.channel');
var partnerId = mc.getProperty('query.param.partnerId');
var brandId = mc.getProperty('query.param.brandId');
var reqTime = mc.getProperty('query.param.reqTime');
var timeType = mc.getProperty('query.param.timeType');
var timeZoneID = mc.getProperty('query.param.timeZoneID');
var accessUser = mc.getProperty('query.param.accessUser');
var accessPassword = mc.getProperty('query.param.accessPassword');
var operatorId = mc.getProperty('query.param.operatorId');
var code = mc.getProperty('query.param.code');
var value = mc.getProperty('query.param.value');
var orderId = mc.getProperty('query.param.orderId');
var custId = mc.getProperty('query.param.custId');
var startTime = mc.getProperty('query.param.startTime');
var endTime = mc.getProperty('query.param.endTime');
var totalRowNum = mc.getProperty('query.param.totalRowNum');
var beginRowNum = mc.getProperty('query.param.beginRowNum');
var fetchRowNum = mc.getProperty('query.param.fetchRowNum');
var additionalPropertyCode = mc.getProperty('query.param.additionalPropertyCode');
var additionalPropertyValue = mc.getProperty('query.param.additionalPropertyValue');


var valueSet = new Array();
var codeSet = new Array();
var additionalPropertyCodeSet = new Array();
var additionalPropertyValueSet = new Array();

if (!!code) {
    codeSet = code.split(",");
}
if (!!value) {
    valueSet = value.split(",");
}
if (!!additionalPropertyCode) {
    additionalPropertyCodeSet = additionalPropertyCode.split(",");
}
if (!!additionalPropertyValue) {
    additionalPropertyValueSet = additionalPropertyValue.split(",");
}


var request = "<ord:QueryOrderReqMsg xmlns:ord=\"http://www.huawei.com/bss/soaif/interface/OrderService/\" xmlns:com=\"http://www.huawei.com/bss/soaif/interface/common/\">" +
    "<com:ReqHeader>";

if ((!!version)) {
    request = request + " <com:Version>" + version + "</com:Version>";
}

if ((!!businessCode)) {
    request = request + "<com:BusinessCode>" + businessCode + "</com:BusinessCode>";
}

if (!!transactionId) {
    request = request + "<com:TransactionId>" + transactionId + "</com:TransactionId>";
}

if ((!!channel)) {
    request = request + " <com:Channel>" + channel + "</com:Channel>";
}

if ((!!partnerId)) {
    request = request + "<com:PartnerId>" + partnerId + "</com:PartnerId>";
}

if ((!!brandId)) {
    request = request + " <com:BrandId>" + brandId + "</com:BrandId>";
}

if (!!reqTime) {
    request = request + "<com:ReqTime>" + reqTime + "</com:ReqTime>";
}

if ((!!timeType)) {
    request = request + "<com:TimeFormat>" +
        "<com:TimeType>timeType</com:TimeType>";

    if ((!!timeZoneID)) {
        request = request + "<com:TimeZoneID>timeZoneID</com:TimeZoneID>";
    }
    request = request + "</com:TimeFormat>";
}

request = request + "<com:AccessUser>" + accessUser + "</com:AccessUser>";
request = request + "<com:AccessPassword>" + accessPassword + "</com:AccessPassword>";

if ((!!operatorId)) {
    request = request + "<com:OperatorId>" + operatorId + "</com:OperatorId>";
}

if ((codeSet.length) == (valueSet.length)) {
    for (i = 0; i < codeSet.length; i++) {
        request = request + "<com:AdditionalProperty> ";
        request = request + "<com:Code>" + codeSet[i] + "</com:Code>";
        request = request + "<com:Value>" + valueSet[i] + "</com:Value>";
        request = request + "</com:AdditionalProperty> ";

    }
}

request = request + "  </com:ReqHeader>";

if (!!orderId) {
    request = request + "<ord:OrderId>" + orderId + "</ord:OrderId>";
}
if (!!custId) {
    request = request + "<ord:CustId>" + custId + "</ord:CustId>";
}

if ((!!startTime) && (!!endTime)) {
    request = request + "<ord:TimePeriod>";
    request = request + "<com:StartTime>" + startTime + "</com:StartTime>";
    request = request + "<com:EndTime>" + endTime + "</com:EndTime>";
    request = request + "</ord:TimePeriod>";

}

if ((!!totalRowNum) && (!!beginRowNum) && (!!fetchRowNum)) {
    request = request + " <ord:PagingInfo>";
    request = request + "<com:TotalRowNum>" + totalRowNum + "</com:TotalRowNum>";
    request = request + "<com:BeginRowNum>" + beginRowNum + "</com:BeginRowNum>";
    request = request + "<com:FetchRowNum>" + fetchRowNum + "</com:FetchRowNum>";
    request = request + " </ord:PagingInfo>";
}

if ((additionalPropertyCodeSet.length) == (additionalPropertyValueSet.length)) {
    for (var i = 0; i < additionalPropertyCodeSet.length; i++) {
        request = request + "<ord:AdditionalProperty>";
        request = request + "<com:Code>" + additionalPropertyCodeSet[i] + "</com:Code>";
        request = request + "<com:Value>" + additionalPropertyValueSet[i] + "</com:Value>";
        request = request + "</ord:AdditionalProperty>";
    }
}

request = request + "</ord:QueryOrderReqMsg>";
mc.setPayloadXML(new XML(request));

