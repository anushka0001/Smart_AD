//Request type: POST ( remove this comment before use in ESB)

var requestObj = mc.getPayloadJSON();
var request = " <ord:CreateOrderReqMsg xmlns:ord=\"http://www.huawei.com/bss/soaif/interface/OrderService/\" xmlns:com=\"http://www.huawei.com/bss/soaif/interface/common/\"> " +
    "<com:ReqHeader> ";
if (typeof (requestObj.ReqHeader.Version) != "undefined") {
    request = request + "<com:Version>" + requestObj.ReqHeader.Version + "</com:Version> ";
}

if (typeof (requestObj.ReqHeader.BusinessCode) != "undefined") {
    request = request + "<com:BusinessCode>" + requestObj.ReqHeader.BusinessCode + "</com:BusinessCode> ";
}

request = request + "<com:TransactionId>" + requestObj.ReqHeader.TransactionId + "</com:TransactionId> ";

if (typeof (requestObj.ReqHeader.Channel) != "undefined") {
    request = request + "<com:Channel>" + requestObj.ReqHeader.Channel + "</com:Channel> ";
}

if (typeof (requestObj.ReqHeader.PartnerId) != "undefined") {
    request = request + "<com:PartnerId>" + requestObj.ReqHeader.PartnerId + "</com:PartnerId> ";
}

if (typeof (requestObj.ReqHeader.BrandId) != "undefined") {
    request = request + "<com:BrandId>" + requestObj.ReqHeader.BrandId + "</com:BrandId> ";
}

request = request + "<com:ReqTime>" + requestObj.ReqHeader.ReqTime + "</com:ReqTime> ";

if (typeof (requestObj.ReqHeader.TimeFormat) != "undefined") {
    request = request + "<com:TimeFormat>";
    request = request + "<com:TimeType>" + requestObj.ReqHeader.TimeFormat.TimeType + "</com:TimeType> ";
    if (typeof (requestObj.ReqHeader.TimeFormat.TimeZoneID) != "undefined") {
        request = request + "<com:TimeZoneID>" + requestObj.ReqHeader.TimeFormat.TimeZoneID + "</com:TimeZoneID> ";
    }
    request = request + "</com:TimeFormat>";
}


request = request + "<com:AccessUser>" + requestObj.ReqHeader.AccessUser + "</com:AccessUser> ";

request = request + "<com:AccessPassword>" + requestObj.ReqHeader.AccessPassword + "</com:AccessPassword> ";

if (typeof (requestObj.ReqHeader.OperatorId) != "undefined") {
    request = request + "<com:OperatorId>" + requestObj.ReqHeader.OperatorId + "</com:OperatorId> ";
}

if (typeof(requestObj.ReqHeader.AdditionalProperty) != "undefined") {
    for (i = 0; i < requestObj.ReqHeader.AdditionalProperty.length; i++) {
        request = request + "<com:AdditionalProperty> " +
            "<com:Code>" + requestObj.ReqHeader.AdditionalProperty[i].Code + "</com:Code>" +
            "<com:Value>" + requestObj.ReqHeader.AdditionalProperty[i].Value + "</com:Value>" +
            "</com:AdditionalProperty>";
    }
}
request = request + "</com:ReqHeader>";

if (typeof (requestObj.Order) != "undefined") {
    request = request + "<ord:Order>";
    if (typeof (requestObj.Order.OrderType) != "undefined") {
        request = request + "<ord:OrderType>" + requestObj.Order.OrderType + "</ord:OrderType>";
    }
    if (typeof (requestObj.Order.ExecuteTime) != "undefined") {
        request = request + "<ord:ExecuteTime>" + requestObj.Order.ExecuteTime + "</ord:ExecuteTime>";
    }
    request = request + "</ord:Order>";
}

if (typeof (requestObj.Customer) != "undefined") {
    request = request + "<ord:Customer>";
    if (typeof (requestObj.Customer.ExistingCustId) != "undefined") {
        request = request + "<ord:ExistingCustId>" + requestObj.Customer.ExistingCustId + "</ord:ExistingCustId>";
    }
    if (typeof (requestObj.Customer.NewCustomer) != "undefined") {
        request = request + "<ord:NewCustomer>";
        if (typeof (requestObj.Customer.NewCustomer.CustLevel) != "undefined") {
            request = request + "<ord:CustLevel>" + requestObj.Customer.NewCustomer.CustLevel + "</ord:CustLevel>";
        }
        if (typeof (requestObj.Customer.NewCustomer.CustSegment) != "undefined") {
            request = request + "<ord:CustSegment>" + requestObj.Customer.NewCustomer.CustSegment + "</ord:CustSegment>";
        }

        if (typeof (requestObj.Customer.NewCustomer.NoticeSuppress) != "undefined") {
            for (var i = 0; i < requestObj.Customer.NewCustomer.NoticeSuppress.length; i++) {
                request = request + "<ord:NoticeSuppress>";
                if (typeof (requestObj.Customer.NewCustomer.NoticeSuppress[i].ChannelType) != "undefined") {
                    request = request + "<ord:ChannelType>" + requestObj.Customer.NewCustomer.NoticeSuppress[i].ChannelType + "</ord:ChannelType>";
                }
                if (typeof (requestObj.Customer.NewCustomer.NoticeSuppress[i].NoticeType) != "undefined") {
                    request = request + "<ord:NoticeType>" + requestObj.Customer.NewCustomer.NoticeSuppress[i].NoticeType + "</ord:NoticeType>";
                }
                if (typeof (requestObj.Customer.NewCustomer.NoticeSuppress[i].SubNoticeType) != "undefined") {
                    request = request + "<ord:SubNoticeType>" + requestObj.Customer.NewCustomer.NoticeSuppress[i].SubNoticeType + "</ord:SubNoticeType>";
                }
                request = request + "</ord:NoticeSuppress>";
            }

        }
        if (typeof (requestObj.Customer.NewCustomer.Title) != "undefined") {
            request = request + "<ord:Title>" + requestObj.Customer.NewCustomer.Title + "</ord:Title>";
        }
        if (typeof (requestObj.Customer.NewCustomer.Name) != "undefined") {
            request = request + "<ord:Name>";
            if (typeof (requestObj.Customer.NewCustomer.Name.FirstName) != "undefined") {
                request = request + "<com:FirstName>" + requestObj.Customer.NewCustomer.Name.FirstName + "</com:FirstName>";
            }
            if (typeof (requestObj.Customer.NewCustomer.Name.MiddleName) != "undefined") {
                request = request + "<com:MiddleName>" + requestObj.Customer.NewCustomer.Name.MiddleName + "</com:MiddleName>";
            }
            if (typeof (requestObj.Customer.NewCustomer.Name.LastName) != "undefined") {
                request = request + "<com:LastName>" + requestObj.Customer.NewCustomer.Name.LastName + "</com:LastName>";
            }

            request = request + "</ord:Name>";
        }
        if (typeof (requestObj.Customer.NewCustomer.Nationality) != "undefined") {
            request = request + " <ord:Nationality>" + requestObj.Customer.NewCustomer.Nationality + "</ord:Nationality>";
        }
        if (typeof (requestObj.Customer.NewCustomer.Certificate) != "undefined") {
            request = request + "<ord:Certificate>";
            if (typeof (requestObj.Customer.NewCustomer.Certificate.IdType) != "undefined") {
                request = request + "<com:IdType>" + requestObj.Customer.NewCustomer.Certificate.IdType + "</com:IdType>";
            }
            if (typeof (requestObj.Customer.NewCustomer.Certificate.IdNum) != "undefined") {
                request = request + "<com:IdNum>" + requestObj.Customer.NewCustomer.Certificate.IdNum + "</com:IdNum>";
            }

            request = request + "</ord:Certificate>";
        }
        if ((typeof (requestObj.Customer.NewCustomer.Birthday) != "undefined")) {
            request = request + "<ord:Birthday>" + requestObj.Customer.NewCustomer.Birthday + "</ord:Birthday>";
        }
        if ((typeof (requestObj.Customer.NewCustomer.Contact) != "undefined")) {
            for (var i = 0; i < requestObj.Customer.NewCustomer.Contact.length; i++) {
                request = request + "<ord:Contact>";
                if (typeof (requestObj.Customer.NewCustomer.Contact[i].ContactId) != "undefined") {
                    request = request + "<com:ContactId>" + requestObj.Customer.NewCustomer.Contact[i].ContactId + "</com:ContactId>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Contact[i].Priority) != "undefined") {
                    request = request + "<com:Priority>" + requestObj.Customer.NewCustomer.Contact[i].Priority + "</com:Priority>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Contact[i].Title) != "undefined") {
                    request = request + "<com:Title>" + requestObj.Customer.NewCustomer.Contact[i].Title + "</com:Title>";
                }

                if (typeof (requestObj.Customer.NewCustomer.Contact[i].Name) != "undefined") {
                    request = request + "<com:Name>";
                    if (typeof (requestObj.Customer.NewCustomer.Contact[i].Name.FirstName) != "undefined") {
                        request = request + "<com:FirstName>" + requestObj.Customer.NewCustomer.Contact[i].Name.FirstName + "</com:FirstName>";
                    }
                    if (typeof (requestObj.Customer.NewCustomer.Contact[i].Name.MiddleName) != "undefined") {
                        request = request + "<com:MiddleName>" + requestObj.Customer.NewCustomer.Contact[i].Name.MiddleName + "</com:MiddleName>";
                    }
                    if (typeof (requestObj.Customer.NewCustomer.Contact[i].Name.LastName) != "undefined") {
                        request = request + "<com:LastName>" + requestObj.Customer.NewCustomer.Contact[i].Name.LastName + "</com:LastName>";
                    }
                    request = request + "</com:Name>";
                }

                if (typeof (requestObj.Customer.NewCustomer.Contact[i].OfficePhone) != "undefined") {
                    request = request + "<com:OfficePhone>" + requestObj.Customer.NewCustomer.Contact[i].OfficePhone + "</com:OfficePhone>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Contact[i].HomePhone) != "undefined") {
                    request = request + "<com:HomePhone>" + requestObj.Customer.NewCustomer.Contact[i].HomePhone + "</com:HomePhone>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Contact[i].MobilePhone) != "undefined") {
                    request = request + "<com:MobilePhone>" + requestObj.Customer.NewCustomer.Contact[i].MobilePhone + "</com:MobilePhone>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Contact[i].Email) != "undefined") {
                    request = request + "<com:Email>" + requestObj.Customer.NewCustomer.Contact[i].Email + "</com:Email>";
                }

                request = request + "</ord:Contact>";
            }
        }

        if (typeof (requestObj.Customer.NewCustomer.Address) != "undefined") {
            for (var i = 0; i < requestObj.Customer.NewCustomer.Address.length; i++) {
                request = request + "<ord:Address>";
                if (typeof (requestObj.Customer.NewCustomer.Address[i].AddressId) != "undefined") {
                    request = request + "<com:AddressId>" + requestObj.Customer.NewCustomer.Address[i].AddressId + "</com:AddressId>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].AddressType) != "undefined") {
                    request = request + "<com:AddressType>" + requestObj.Customer.NewCustomer.Address[i].AddressType + "</com:AddressType>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address1) != "undefined") {
                    request = request + "<com:Address1>" + requestObj.Customer.NewCustomer.Address[i].Address1 + "</com:Address1>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address2) != "undefined") {
                    request = request + "<com:Address2>" + requestObj.Customer.NewCustomer.Address[i].Address2 + "</com:Address2>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address3) != "undefined") {
                    request = request + "<com:Address3>" + requestObj.Customer.NewCustomer.Address[i].Address3 + "</com:Address3>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address4) != "undefined") {
                    request = request + "<com:Address4>" + requestObj.Customer.NewCustomer.Address[i].Address4 + "</com:Address4>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address5) != "undefined") {
                    request = request + "<com:Address5>" + requestObj.Customer.NewCustomer.Address[i].Address5 + "</com:Address5>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address6) != "undefined") {
                    request = request + "<com:Address6>" + requestObj.Customer.NewCustomer.Address[i].Address6 + "</com:Address6>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address7) != "undefined") {
                    request = request + "<com:Address7>" + requestObj.Customer.NewCustomer.Address[i].Address7 + "</com:Address7>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address8) != "undefined") {
                    request = request + "<com:Address8>" + requestObj.Customer.NewCustomer.Address[i].Address8 + "</com:Address8>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address9) != "undefined") {
                    request = request + "<com:Address9>" + requestObj.Customer.NewCustomer.Address[i].Address9 + "</com:Address9>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address10) != "undefined") {
                    request = request + "<com:Address10>" + requestObj.Customer.NewCustomer.Address[i].Address10 + "</com:Address10>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address11) != "undefined") {
                    request = request + "<com:Address11>" + requestObj.Customer.NewCustomer.Address[i].Address11 + "</com:Address11>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].Address12) != "undefined") {
                    request = request + "<com:Address12>" + requestObj.Customer.NewCustomer.Address[i].Address12 + "</com:Address12>";
                }
                if (typeof (requestObj.Customer.NewCustomer.Address[i].PostCode) != "undefined") {
                    request = request + "<com:PostCode>" + requestObj.Customer.NewCustomer.Address[i].PostCode + "</com:PostCode>";
                }

                request = request + "</ord:Address>";
            }
        }

        if (typeof (requestObj.Customer.NewCustomer.Status) != "undefined") {
            request = request + "<ord:Status>" + requestObj.Customer.NewCustomer.Statuse + "</ord:Status>";
        }

        if (typeof(requestObj.Customer.NewCustomer.AdditionalProperty) != "undefined") {
            for (i = 0; i < requestObj.Customer.NewCustomer.AdditionalProperty.length; i++) {
                request = request + "<com:AdditionalProperty> " +
                    "<com:Code>" + requestObj.Customer.NewCustomer.AdditionalProperty[i].Code + "</com:Code>" +
                    "<com:Value>" + requestObj.Customer.NewCustomer.AdditionalProperty[i].Value + "</com:Value>" +
                    "</com:AdditionalProperty>";
            }
        }
        request = request + "</ord:NewCustomer>";
    }
    request = request + "</ord:Customer>";
}

if (typeof (requestObj.Account) != "undefined") {
    for (var i = 0; i < 2; i++) {
        request = request + "<ord:Account>";
        if (typeof (requestObj.Account[i].ExistingAcctId) != "undefined") {
            request = request + "<ord:ExistingAcctId>" + requestObj.Account[i].ExistingAcctId + "</ord:ExistingAcctId>";
        }
        if (typeof (requestObj.Account[i].NewAccount) != "undefined") {
            request = request + "<ord:NewAccount>";
            if (typeof (requestObj.Account[i].NewAccount.AcctName) != "undefined") {
                request = request + "<ord:AcctName>" + requestObj.Account[i].NewAccount.AcctName + "</ord:AcctName>";
            }
            if (typeof (requestObj.Account[i].NewAccount.PaymentType) != "undefined") {
                request = request + "<ord:PaymentType>" + requestObj.Account[i].NewAccount.PaymentType + "</ord:PaymentType>";
            }
            if (typeof (requestObj.Account[i].NewAccount.Title) != "undefined") {
                request = request + "<ord:Title>" + requestObj.Account[i].NewAccount.Title + "</ord:Title>";
            }

            if (typeof (requestObj.Account[i].NewAccount.Name) != "undefined") {
                request = request + "<ord:Name>";
                if (typeof (requestObj.Account[i].NewAccount.Name.FirstName) != "undefined") {
                    request = request + "<com:FirstName>" + requestObj.Account[i].NewAccount.Name.FirstName + "</com:FirstName>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Name.MiddleName) != "undefined") {
                    request = request + "<com:MiddleName>" + requestObj.Account[i].NewAccount.Name.MiddleName + "</com:MiddleName>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Name.LastName) != "undefined") {
                    request = request + "<com:LastName>" + requestObj.Account[i].NewAccount.Name.LastName + "</com:LastName>";
                }

                request = request + "</ord:Name>";
            }
            if (typeof (requestObj.Account[i].NewAccount.BillCycleType) != "undefined") {
                request = request + " <ord:BillCycleType>" + requestObj.Account[i].NewAccount.BillCycleType + "</ord:BillCycleType>";
            }
            if (typeof (requestObj.Account[i].NewAccount.BillLanguage) != "undefined") {
                request = request + " <ord:BillLanguage>" + requestObj.Account[i].NewAccount.BillLanguage + "</ord:BillLanguage>";
            }

            if (typeof (requestObj.Account[i].NewAccount.Contact) != "undefined") {
                request = request + "<ord:Contact>";
                if (typeof (requestObj.Account[i].NewAccount.Contact.ContactId) != "undefined") {
                    request = request + " <com:ContactId>" + requestObj.Account[i].NewAccount.Contact.ContactId + "</com:ContactId>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Contact.Priority) != "undefined") {
                    request = request + " <com:Priority>" + requestObj.Account[i].NewAccount.Contact.Priority + "</com:Priority>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Contact.ContactType) != "undefined") {
                    request = request + " <com:ContactType>" + requestObj.Account[i].NewAccount.Contact.ContactType + "</com:ContactType>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Contact.Title) != "undefined") {
                    request = request + " <com:Title>" + requestObj.Account[i].NewAccount.Contact.Title + "</com:Title>";
                }

                if (typeof (requestObj.Account[i].NewAccount.Contact.Name) != "undefined") {
                    request = request + "<com:Name>";
                    if (typeof (rrequestObj.Account[i].NewAccount.Contact.Name.FirstName) != "undefined") {
                        request = request + "<com:FirstName>" + requestObj.Account[i].NewAccount.Contact.Name.FirstName + "</com:FirstName>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Contactt.Name.MiddleName) != "undefined") {
                        request = request + "<com:MiddleName>" + requestObj.Account[i].NewAccount.Contact.Name.MiddleName + "</com:MiddleName>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Contact.Name.LastName) != "undefined") {
                        request = request + "<com:LastName>" + requestObj.Account[i].NewAccount.Contact.Name.LastName + "</com:LastName>";
                    }

                    request = request + "</com:Name>";
                }

                if (typeof (requestObj.Account[i].NewAccount.Contact.OfficePhone) != "undefined") {
                    request = request + "<com:OfficePhone>" + requestObj.Account[i].NewAccount.Contact.OfficePhone + "</com:OfficePhone>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Contact.HomePhone) != "undefined") {
                    request = request + "<com:HomePhone>" + requestObj.Account[i].NewAccount.Contact.HomePhone + "</com:HomePhone>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Contact.MobilePhone) != "undefined") {
                    request = request + "<com:MobilePhone>" + requestObj.Account[i].NewAccount.Contact.MobilePhone + "</com:MobilePhone>";
                }
                if (typeof (requestObj.Account[i].NewAccount.Contact.Email) != "undefined") {
                    request = request + "<com:Email>" + requestObj.Account[i].NewAccount.Contact.Email + "</com:Email>";
                }

                request = request + "</ord:Contact>";
            }

            if (typeof (requestObj.Account[i].NewAccount.Address) != "undefined") {
                for (var j = 0; j < requestObj.Account[i].NewAccount.Address.length; j++) {
                    request = request + "<ord:Address>";
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].AddressId) != "undefined") {
                        request = request + "<com:AddressId>" + requestObj.Account[i].NewAccount.Address[j].AddressId + "</com:AddressId>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].AddressType) != "undefined") {
                        request = request + "<com:AddressType>" + requestObj.Account[i].NewAccount.Address[j].AddressType + "</com:AddressType>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address1) != "undefined") {
                        request = request + "<com:Address1>" + requestObj.Account[i].NewAccount.Address[j].Address1 + "</com:Address1>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address2) != "undefined") {
                        request = request + "<com:Address2>" + requestObj.Account[i].NewAccount.Address[j].Address2 + "</com:Address2>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address3) != "undefined") {
                        request = request + "<com:Address3>" + requestObj.Account[i].NewAccount.Address[j].Address3 + "</com:Address3>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address4) != "undefined") {
                        request = request + "<com:Address4>" + requestObj.Account[i].NewAccount.Address[j].Address4 + "</com:Address4>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address5) != "undefined") {
                        request = request + "<com:Address5>" + requestObj.Account[i].NewAccount.Address[j].Address5 + "</com:Address5>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address6) != "undefined") {
                        request = request + "<com:Address6>" + requestObj.Account[i].NewAccount.Address[j].Address6 + "</com:Address6>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address7) != "undefined") {
                        request = request + "<com:Address7>" + requestObj.Account[i].NewAccount.Address[j].Address7 + "</com:Address7>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address8) != "undefined") {
                        request = request + "<com:Address8>" + requestObj.Account[i].NewAccount.Address[j].Address8 + "</com:Address8>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address9) != "undefined") {
                        request = request + "<com:Address9>" + requestObj.Account[i].NewAccount.Address[j].Address9 + "</com:Address9>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address10) != "undefined") {
                        request = request + "<com:Address10>" + requestObj.Account[i].NewAccount.Address[j].Address10 + "</com:Address10>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address11) != "undefined") {
                        request = request + "<com:Address11>" + requestObj.Account[i].NewAccount.Address[j].Address11 + "</com:Address11>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].Address12) != "undefined") {
                        request = request + "<com:Address12>" + requestObj.Account[i].NewAccount.Address[j].Address12 + "</com:Address12>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.Address[j].PostCode) != "undefined") {
                        request = request + "<com:PostCode>" + requestObj.Account[i].NewAccount.Address[j].PostCode + "</com:PostCode>";
                    }

                    request = request + "</ord:Address>";
                }
            }

            if (typeof (requestObj.Account[i].NewAccount.BillMedium) != "undefined") {
                for (var j = 0; j < requestObj.Account[i].NewAccount.BillMedium.length; j++) {
                    request = request + "<ord:BillMedium>";
                    if (typeof (requestObj.Account[i].NewAccount.BillMedium[j].BillMediumId) != "undefined") {
                        request = request + "<com:BillMediumId>" + requestObj.Account[i].NewAccount.BillMedium[j].BillMediumId + "</com:BillMediumId>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.BillMedium[j].BillMediumCode) != "undefined") {
                        request = request + "<com:BillMediumCode>" + requestObj.Account[i].NewAccount.BillMedium[j].BillMediumCode + "</com:BillMediumCode>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.BillMedium[j].BillContentType) != "undefined") {
                        request = request + "<com:BillContentType>" + requestObj.Account[i].NewAccount.BillMedium[j].BillContentType + "</com:BillContentType>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.BillMedium[j].BillMediumInfo) != "undefined") {
                        request = request + "<com:BillMediumInfo>" + requestObj.Account[i].NewAccount.BillMedium[j].BillMediumInfo + "</com:BillMediumInfo>";
                    }

                    request = request + "</ord:BillMedium>";
                }
            }

            if (typeof (requestObj.Account[i].NewAccount.Currency) != "undefined") {
                request = request + "<ord:Currency>" + requestObj.Account[i].NewAccount.Currency + "</ord:Currency>";
            }
            if (typeof (requestObj.Account[i].NewAccount.InitialBalance) != "undefined") {
                request = request + "<ord:InitialBalance>" + requestObj.Account[i].NewAccount.InitialBalance + "</ord:InitialBalance>";
            }

            if (typeof (requestObj.Account[i].NewAccount.CreditLimit) != "undefined") {
                for (var j = 0; j < requestObj.Account[i].NewAccount.CreditLimit.length; j++) {
                    request = request + "<ord:CreditLimit>";
                    if (typeof (requestObj.Account[i].NewAccount.CreditLimit[j].LimitType) != "undefined") {
                        request = request + " <com:LimitType>" + requestObj.Account[i].NewAccount.CreditLimit[j].LimitType + "</com:LimitType>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.CreditLimit[j].LimitValue) != "undefined") {
                        request = request + " <com:LimitValue>" + requestObj.Account[i].NewAccount.CreditLimit[j].LimitValue + "</com:LimitValue>";
                    }
                    request = request + "</ord:CreditLimit>";
                }
            }

            if (typeof (requestObj.Account[i].NewAccount.AcctPayMethod) != "undefined") {
                request = request + " <ord:AcctPayMethod>" + requestObj.Account[i].NewAccount.AcctPayMethod + "</ord:AcctPayMethod>";
            }

            if (typeof (requestObj.Account[i].NewAccount.PaymentChannel) != "undefined") {
                for (var j = 0; j < requestObj.Account[i].NewAccount.PaymentChannel.length; j++) {
                    request = request + "<ord:PaymentChannel>";
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].PaymentId) != "undefined") {
                        request = request + " <com:PaymentId>" + requestObj.Account[i].NewAccount.PaymentChannel[j].PaymentId + "</com:PaymentId>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].AcctType) != "undefined") {
                        request = request + " <com:AcctType>" + requestObj.Account[i].NewAccount.PaymentChannel[j].AcctType + "</com:AcctType>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].AcctNum) != "undefined") {
                        request = request + " <com:AcctNum>" + requestObj.Account[i].NewAccount.PaymentChannel[j].AcctNum + "</com:AcctNum>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].AcctName) != "undefined") {
                        request = request + " <com:AcctName>" + requestObj.Account[i].NewAccount.PaymentChannel[j].AcctName + "</com:AcctName>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].BankCode) != "undefined") {
                        request = request + " <com:BankCode>" + requestObj.Account[i].NewAccount.PaymentChannel[j].BankCode + "</com:BankCode>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].BankBranchCode) != "undefined") {
                        request = request + " <com:BankBranchCode>" + requestObj.Account[i].NewAccount.PaymentChannel[j].BankBranchCode + "</com:BankBranchCode>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].CreditCardType) != "undefined") {
                        request = request + " <com:CreditCardType>" + requestObj.Account[i].NewAccount.PaymentChannel[j].CreditCardType + "</com:CreditCardType>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].CVVNum) != "undefined") {
                        request = request + " <com:CVVNum>" + requestObj.Account[i].NewAccount.PaymentChannel[j].CVVNum + "</com:CVVNum>";
                    }
                    if (typeof (requestObj.Account[i].NewAccount.PaymentChannel[j].ExpireDate) != "undefined") {
                        request = request + " <com:ExpireDate>" + requestObj.Account[i].NewAccount.PaymentChannel[j].ExpireDate + "</com:ExpireDate>";
                    }

                    request = request + "</ord:PaymentChannel>";
                }
            }

            if (typeof(requestObj.Account[i].NewAccount.AdditionalProperty) != "undefined") {
                for (j = 0; j < requestObj.Account[i].NewAccount.AdditionalProperty.length; j++) {
                    request = request + "<com:AdditionalProperty> " +
                        "<com:Code>" + requestObj.Account[i].NewAccount.AdditionalProperty[j].Code + "</com:Code>" +
                        "<com:Value>" + requestObj.Account[i].NewAccount.AdditionalProperty[j].Value + "</com:Value>" +
                        "</com:AdditionalProperty>";
                }
            }
            if (typeof (requestObj.Account[i].NewAccount.IsDefaultAcct) != "undefined") {
                request = request + "<ord:IsDefaultAcct>" + requestObj.Account[i].NewAccount.IsDefaultAcct + "</ord:IsDefaultAcct>";
            }

            request = request + "</ord:NewAccount>";
        }
        request = request + "</ord:Account>";
    }
}

if (typeof (requestObj.Subscriber) != "undefined") {
    request = request + "<ord:Subscriber>";
    if (typeof (requestObj.Subscriber.BrandId) != "undefined") {
        request = request + "<ord:BrandId>" + requestObj.Subscriber.BrandId + "</ord:BrandId>";
    }
    if (typeof (requestObj.Subscriber.ServiceNum) != "undefined") {
        request = request + "<ord:ServiceNum>" + requestObj.Subscriber.ServiceNum + "</ord:ServiceNum>";
    }
    if (typeof (requestObj.Subscriber.ICCID) != "undefined") {
        request = request + "<ord:ICCID>" + requestObj.Subscriber.ICCID + "</ord:ICCID>";
    }
    if (typeof (requestObj.Subscriber.Language) != "undefined") {
        request = request + "<ord:Language>" + requestObj.Subscriber.Language + "</ord:Language>";
    }
    if (typeof (requestObj.Subscriber.WrittenLanguage) != "undefined") {
        request = request + "<ord:WrittenLanguage>" + requestObj.Subscriber.WrittenLanguage + "</ord:WrittenLanguage>";
    }
    if (typeof (requestObj.Subscriber.Password) != "undefined") {
        request = request + "<ord:Password>" + requestObj.Subscriber.Password + "</ord:Password>";
    }
    if (typeof (requestObj.Subscriber.IMEI) != "undefined") {
        request = request + "<ord:IMEI>" + requestObj.Subscriber.IMEI + "</ord:IMEI>";
    }
    if (typeof (requestObj.Subscriber.IMSI) != "undefined") {
        request = request + "<ord:IMSI>" + requestObj.Subscriber.IMSI + "</ord:IMSI>";
    }

    if (typeof (requestObj.Subscriber.AdditionalProperty) != "undefined") {
        for (var i = 0; i < requestObj.Subscriber.AdditionalProperty.length; i++) {
            request = request + "  <com:AdditionalProperty>";
            request = request + "<com:Code>" + requestObj.Subscriber.AdditionalProperty[i].Code + "</com:Code>";
            request = request + "<com:Value>" + requestObj.Subscriber.AdditionalProperty[i].Value + "</com:Value>";
            request = request + "  </com:AdditionalProperty>";
        }
    }
    request = request + "</ord:Subscriber>";
}

if (typeof (requestObj.PrimaryOffering) != "undefined") {
    request = request + "<ord:PrimaryOffering>";
    if (typeof (requestObj.PrimaryOffering.OfferingId) != "undefined") {
        request = request + "<com:OfferingId>";
        if (typeof (requestObj.PrimaryOffering.OfferingId.OfferingId) != "undefined") {
            request = request + "<com:OfferingId>" + requestObj.PrimaryOffering.OfferingId.OfferingId + "</com:OfferingId>";
        }
        if (typeof (requestObj.PrimaryOffering.OfferingId.PurchaseSeq) != "undefined") {
            request = request + "<com:PurchaseSeq>" + requestObj.PrimaryOffering.OfferingId.PurchaseSeq + "</com:PurchaseSeq>";
        }
        request = request + "</com:OfferingId>";
    }
    if (requestObj.PrimaryOffering.Contract) {
        request = request + "<com:Contract>";
        if (typeof (requestObj.PrimaryOffering.Contract.AgreementId) != "undefined") {
            request = request + "<com:AgreementId>" + requestObj.PrimaryOffering.Contract.AgreementId + "</com:AgreementId>";
        }
        if (typeof (requestObj.PrimaryOffering.Contract.DurationUnit) != "undefined") {
            request = request + "<com:DurationUnit>" + requestObj.PrimaryOffering.Contract.DurationUnit + "</com:DurationUnit>";
        }
        if (typeof (requestObj.PrimaryOffering.Contract.DurationValue) != "undefined") {
            request = request + "<com:DurationValue>" + requestObj.PrimaryOffering.Contract.DurationValue + "</com:DurationValue>";
        }
        if (typeof (requestObj.PrimaryOffering.Contract.Prolongation) != "undefined") {
            for (var i = 0; i < requestObj.PrimaryOffering.Contract.Prolongation.length; i++) {
                if (typeof (requestObj.PrimaryOffering.Contract.Prolongation[i].DurationID) != "undefined") {
                    request = request + " <com:DurationID>" + requestObj.PrimaryOffering.Contract.Prolongation[i].DurationID + "</com:DurationID>";
                }
                if (typeof (requestObj.PrimaryOffering.Contract.Prolongation[i].DurationUnit) != "undefined") {
                    request = request + " <com:DurationUnit>" + requestObj.PrimaryOffering.Contract.Prolongation[i].DurationUnit + "</com:DurationUnit>";
                }
                if (typeof (requestObj.PrimaryOffering.Contract.Prolongation[i].DurationValue) != "undefined") {
                    request = request + " <com:DurationValue>" + requestObj.PrimaryOffering.Contract.Prolongation[i].DurationValue + "</com:DurationValue>";
                }
            }
        }
        if (typeof (requestObj.PrimaryOffering.Contract.AdditionalProperty) != "undefined") {
            for (var i = 0; i < requestObj.PrimaryOffering.Contract.AdditionalProperty.length; i++) {
                request = request + "  <com:AdditionalProperty>";
                request = request + "<com:Code>" + requestObj.PrimaryOffering.Contract.AdditionalProperty[i].Code + "</com:Code>";
                request = request + "<com:Value>" + requestObj.PrimaryOffering.Contract.AdditionalProperty[i].Value + "</com:Value>";
                request = request + "  </com:AdditionalProperty>";
            }
        }
        request = request + "</com:Contract>";
    }

    if (typeof (requestObj.PrimaryOffering.InstanceProperty) != "undefined") {
        for (var i = 0; i < requestObj.PrimaryOffering.InstanceProperty.length; i++) {
            request = request + " <com:InstanceProperty>";
            if (typeof (requestObj.PrimaryOffering.InstanceProperty[i].PropertyCode) != "undefined") {
                request = request + "<com:PropertyCode>" + requestObj.PrimaryOffering.InstanceProperty[i].PropertyCode + "</com:PropertyCode>";
            }
            if (typeof (requestObj.PrimaryOffering.InstanceProperty[i].PropertyType) != "undefined") {
                request = request + "<com:PropertyType>" + requestObj.PrimaryOffering.InstanceProperty[i].PropertyType + "</com:PropertyType>";
            }
            if (typeof (requestObj.PrimaryOffering.InstanceProperty[i].Value) != "undefined") {
                request = request + "<com:Value>" + requestObj.PrimaryOffering.InstanceProperty[i].Value + "</com:Value>";
            }

            if (typeof (requestObj.PrimaryOffering.InstanceProperty[i].SubPropertyInstance) != "undefined") {
                for (var j = 0; j < requestObj.PrimaryOffering.InstanceProperty[i].SubPropertyInstance.length; j++) {
                    request = request + "<com:SubPropertyInstance>";
                    if (typeof (requestObj.PrimaryOffering.InstanceProperty[i].SubPropertyInstance[j].SubPropertyCode) != "undefined") {
                        request = request + "<com:SubPropertyCode>" + requestObj.PrimaryOffering.InstanceProperty[i].SubPropertyInstance[j].SubPropertyCode + "</com:SubPropertyCode>";
                    }
                    if (typeof (requestObj.PrimaryOffering.InstanceProperty[i].SubPropertyInstance[j].Value) != "undefined") {
                        request = request + "<com:Value>" + requestObj.PrimaryOffering.InstanceProperty[i].SubPropertyInstance[j].Value + "</com:Value>";
                    }
                    request = request + "</com:SubPropertyInstance>";
                }
            }

            request = request + " </com:InstanceProperty>";
        }
    }

    request = request + "</ord:PrimaryOffering>";
}

if (typeof (requestObj.SupplementaryOffering) != "undefined") {
    for (var i = 0; i < requestObj.SupplementaryOffering.length; i++) {
        request = request + "<ord:SupplementaryOffering>";
        request = request + " <com:OfferingId>";
        if (typeof (requestObj.SupplementaryOffering[i].OfferingId) != "undefined") {
            request = request + "<com:OfferingId>" + requestObj.SupplementaryOffering[i].OfferingId + "</com:OfferingId>";
        }
        if (typeof (requestObj.SupplementaryOffering[i].PurchaseSeq) != "undefined") {
            request = request + "<com:PurchaseSeq>" + requestObj.SupplementaryOffering[i].PurchaseSeq + "</com:PurchaseSeq>";
        }

        request = request + " </com:OfferingId>";

        if (typeof (requestObj.SupplementaryOffering[i].Contract) != "undefined") {
            request = request + "<com:Contract>";
            if (typeof (requestObj.SupplementaryOffering[i].Contract.AgreementId) != "undefined") {
                request = request + " <com:AgreementId>" + requestObj.SupplementaryOffering[i].Contract.AgreementId + "</com:AgreementId>";
            }
            if (typeof (requestObj.SupplementaryOffering[i].Contract.DurationUnit) != "undefined") {
                request = request + " <com:DurationUnit>" + requestObj.SupplementaryOffering[i].Contract.DurationUnit + "</com:DurationUnit>";
            }
            if (typeof (requestObj.SupplementaryOffering[i].Contract.DurationValue) != "undefined") {
                request = request + " <com:DurationValue>" + requestObj.SupplementaryOffering[i].Contract.DurationValue + "</com:DurationValue>";
            }
            if (typeof (requestObj.SupplementaryOffering[i].Contract.Prolongation) != "undefined") {
                for (var j = 0; j < requestObj.SupplementaryOffering[i].Contract.Prolongation.length; j++) {
                    request = request + " <com:Prolongation>";
                    if (typeof (requestObj.SupplementaryOffering[i].Contract.Prolongation[j].DurationID) != "undefined") {
                        request = request + " <com:DurationID>" + requestObj.SupplementaryOffering[i].Contract.Prolongation[j].DurationID + "</com:DurationID>";
                    }
                    if (typeof (requestObj.SupplementaryOffering[i].Contract.Prolongation[j].DurationUnit) != "undefined") {
                        request = request + " <com:DurationUnit>" + requestObj.SupplementaryOffering[i].Contract.Prolongation[j].DurationUnit + "</com:DurationUnit>";
                    }
                    if (typeof (requestObj.SupplementaryOffering[i].Contract.Prolongation[j].DurationValue) != "undefined") {
                        request = request + " <com:DurationValue>" + requestObj.SupplementaryOffering[i].Contract.Prolongation[j].DurationValue + "</com:DurationValue>";
                    }

                    request = request + " </com:Prolongation>";
                }
            }
            if (typeof (requestObj.SupplementaryOffering[i].Contract.AdditionalProperty) != "undefined") {
                for (var j = 0; j < requestObj.SupplementaryOffering[i].Contract.AdditionalProperty.length; j++) {
                    request = request + "  <com:AdditionalProperty>";
                    request = request + "<com:Code>" + requestObj.SupplementaryOffering[i].Contract.AdditionalProperty[j].Code + "</com:Code>";
                    request = request + "<com:Value>" + requestObj.SupplementaryOffering[i].Contract.AdditionalProperty[j] + "</com:Value>";
                    request = request + "  </com:AdditionalProperty>";
                }
            }


            request = request + "</com:Contract>";
        }

        if (typeof (requestObj.SupplementaryOffering[i].InstanceProperty) != "undefined") {
            for (var j = 0; j < requestObj.SupplementaryOffering[i].InstanceProperty.length; j++) {
                request = request + "<com:InstanceProperty>";
                if (typeof (requestObj.SupplementaryOffering[i].InstanceProperty[j].PropertyCode) != "undefined") {
                    request = request + "<com:PropertyCode>" + requestObj.SupplementaryOffering[i].InstanceProperty[j].PropertyCode + "</com:PropertyCode>";
                }
                if (typeof (requestObj.SupplementaryOffering[i].InstanceProperty[j].PropertyType) != "undefined") {
                    request = request + "<com:PropertyType>" + requestObj.SupplementaryOffering[i].InstanceProperty[j].PropertyType + "</com:PropertyType>";
                }
                if (typeof (requestObj.SupplementaryOffering[i].InstanceProperty[j].Value) != "undefined") {
                    request = request + "<com:Value>" + requestObj.SupplementaryOffering[i].InstanceProperty[j].Value + "</com:Value>";
                }

                for (var k = 0; k < requestObj.SupplementaryOffering[i].InstanceProperty[j].SubPropertyInstance.length; k++) {
                    request = request + "<com:SubPropertyInstance>";
                    if (typeof (requestObj.SupplementaryOffering[i].InstanceProperty[j].SubPropertyInstance[k].SubPropertyCode) != "undefined") {
                        request = request + "<com:SubPropertyCode>" + requestObj.SupplementaryOffering[i].InstanceProperty[j].SubPropertyInstance[k].SubPropertyCode + "</com:SubPropertyCode>";
                    }
                    if (typeof (requestObj.SupplementaryOffering[i].InstanceProperty[j].SubPropertyInstance[k].Value) != "undefined") {
                        request = request + "<com:Value>" + requestObj.SupplementaryOffering[i].InstanceProperty[j].SubPropertyInstance[k].Value + "</com:Value>";
                    }
                    request = request + "</com:SubPropertyInstance>";
                }

                request = request + "</com:InstanceProperty>";
            }
        }

        if (typeof (requestObj.SupplementaryOffering[i].EffectiveMode) != "undefined") {
            request = request + "<ord:EffectiveMode>";
            if (typeof (requestObj.SupplementaryOffering[i].EffectiveMode.Mode) != "undefined") {
                request = request + " <com:Mode>" + requestObj.SupplementaryOffering[i].EffectiveMode.Mode + "</com:Mode>";
            }
            if (typeof (requestObj.SupplementaryOffering[i].EffectiveMode.EffectiveDate) != "undefined") {
                request = request + " <com:EffectiveDate>" + requestObj.SupplementaryOffering[i].EffectiveMode.EffectiveDate + "</com:EffectiveDate>";
            }
            request = request + "</ord:EffectiveMode>";
        }

        if (typeof (requestObj.SupplementaryOffering[i].ExpirationDate) != "undefined") {
            request = request + "<ord:ExpirationDate>" + requestObj.SupplementaryOffering[i].ExpirationDate + "</ord:ExpirationDate>";
        }
        if (typeof (requestObj.SupplementaryOffering[i].ActiveMode) != "undefined") {
            request = request + "<ord:ActiveMode>";
            if (typeof (requestObj.SupplementaryOffering[i].ActiveMode.Mode) != "undefined") {
                request = request + "<com:Mode>" + requestObj.SupplementaryOffering[i].ActiveMode.Mode + "</com:Mode>";
            }
            if (typeof (requestObj.SupplementaryOffering[i].ActiveMode.ActiveDate) != "undefined") {
                request = request + "<com:ActiveDate>" + requestObj.SupplementaryOffering[i].ActiveMode.ActiveDate + "</com:ActiveDate>";
            }
            if (typeof (requestObj.SupplementaryOffering[i].ActiveMode.ActiveTimeLimit) != "undefined") {
                request = request + "<com:ActiveTimeLimit>" + requestObj.SupplementaryOffering[i].ActiveMode.ActiveTimeLimit + "</com:ActiveTimeLimit>";
            }

            request = request + "<ord:ActiveMode>";
        }


        request = request + "</ord:SupplementaryOffering>";
    }
}

if (typeof (requestObj.BusinessFee) != "undefined") {
    for (var i = 0; i < requestObj.BusinessFee.length; i++) {
        request = request + "<ord:BusinessFee>";
        if (typeof (requestObj.BusinessFee[i].ChargeType) != "undefined") {
            request = request + " <com:ChargeType>" + requestObj.BusinessFee[i].ChargeType + "</com:ChargeType>";
        }
        if (typeof (requestObj.BusinessFee[i].Currency) != "undefined") {
            request = request + " <com:Currency>" + requestObj.BusinessFee[i].Currency + "</com:Currency>";
        }
        if (typeof (requestObj.BusinessFee[i].Amount) != "undefined") {
            request = request + " <com:Amount>" + requestObj.BusinessFee[i].Amount + "</com:Amount>";
        }
        if (typeof (requestObj.BusinessFee[i].AdditionalProperty) != "undefined") {
            for (var j = 0; j < requestObj.BusinessFee[i].AdditionalProperty.length; j++) {
                request = request + "  <com:AdditionalProperty>";
                request = request + "<com:Code>" + requestObj.BusinessFee[i].AdditionalProperty[j].Code + "</com:Code>";
                request = request + "<com:Value>" + requestObj.BusinessFee[i].AdditionalProperty[j] + "</com:Value>";
                request = request + "  </com:AdditionalProperty>";
            }
        }
        request = request + "</ord:BusinessFee>";
    }
}

request = request + " </ord:CreateOrderReqMsg>";

mc.setPayloadXML(new XML(request));


